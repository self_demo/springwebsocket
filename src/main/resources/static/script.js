$(document).ready(function() {

});

var stompClient = null;

function connect() {
	let socket = new SockJS("/server1");
	stompClient = Stomp.over(socket);

	stompClient.connect({}, (frame) => {
		console.log('Connected: ' + frame);

		$('#name-from').addClass('d-none');
		$('#chat-room').removeClass('d-none'); // Ensure this line correctly removes the d-none class


		stompClient.subscribe("/topic/return-to", (response) => {
			showMessage(JSON.parse(response.body))
		})

	}, (error) => {
		console.log('Connection error: ' + error);
	});
}

function showMessage(message) {
	let rowHtml = `<tr class="table-animate"><td><b>${message.name} :</b> ${message.content}</td></tr>`;

	// Prepend the row to the table
	$('#message-container-table').prepend(rowHtml);

}


function sendMessage() {

	let jsonOb = {
		name: localStorage.getItem("name"),
		content: $('#message-value').val()
	}

	stompClient.send("/app/message", {}, JSON.stringify(jsonOb))
}